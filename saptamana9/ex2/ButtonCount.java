package ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ButtonCount extends JFrame{

    JTextArea tArea;
    JButton bLoghin;

    ButtonCount(){
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,400);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);

        bLoghin = new JButton("Press This Button");
        bLoghin.setBounds(10,50,250, 20);

        bLoghin.addActionListener(new TratareButon());

        tArea = new JTextArea();
        tArea.setBounds(10,200,250,80);

        add(bLoghin);
        add(tArea);

    }

    public static void main(String[] args) {
        new ButtonCount();
    }

    class TratareButon implements ActionListener{
        private int counter = 0;
        public void actionPerformed(ActionEvent e) {

            counter++;
            ButtonCount.this.tArea.setText("");
            ButtonCount.this.tArea.append("Count:"+counter+"\n"); }
        }
    }