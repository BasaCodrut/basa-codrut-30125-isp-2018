package ex4;

import java.util.Random;

public class Robot extends Thread {
	int x,y;
	String name;
	static Robot r[]= new Robot[10];
	
	Robot(int x,int y,String name){
		this.x=x;
		this.y=y;
		this.name=name;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;	
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getRobot() {
		return name;
	}
	public void setRobot(String name) {
		this.name=name;
	}
	
	public void run() {
		while(true) {
		 for(int i=0;i<10;i++)
		 {	if(r[i]!=null) {
			 Random rand = new Random();
		 	int limita=10;
		 	int ResultX = rand.nextInt(3);
		 	int ResultY = rand.nextInt(3);
		 	int a=r[i].getX()+1-ResultX;
		 	int b=r[i].getY()+1-ResultY;
		 	if((a<0)||(a>=limita))
		 	{
		 		while((a<0)||(a>=limita))
		 		{
		 			int ResultX1 = rand.nextInt(3);
				 	a=r[i].getX()+1-ResultX1;
		 		}
		 	}
		 	else
			 r[i].setX(b);
		 	if((b<0)||(b>=limita))
		 	{
		 		while((b<0)||(b>=limita))
		 		{
		 			int ResultY1 = rand.nextInt(3);
				 	b=r[i].getY()+1-ResultY1;
		 		}
		 	}
		 	else
			 r[i].setY(b);
		 }
		 }
		
		try {
			for(int i=0;i<9;i++) {
				for(int j=i+1;j<10;j++) {
					if(r[i]!=null&&r[j]!=null) {
					System.out.println("X:Compare "+r[i].getX()+"  with  "+r[j].getX());
					System.out.println("Y:Compare "+r[i].getY()+"  with  "+r[j].getY());
					}
					if(r[i]!=null&&r[j]!=null&&(r[i].getX()==r[j].getX())&&(r[i].getY()==r[j].getY())) {
							System.out.println(r[i].getRobot()+" and "+r[j].getRobot()+" were destroyed\n");
										r[i]=null;r[j]=null;
					}
				}
			}
	         Thread.sleep(2000);
	   } catch (InterruptedException e) {
	         e.printStackTrace();
	   }
	}
	}
	public static void main(String[] args) {
		 for(int i=0;i<10;i++)
		 {	if(r[i]==null) {Random rand = new Random();
		 	int ResultX = rand.nextInt(10);
		 	int ResultY = rand.nextInt(10);
		 	r[i]=new Robot(ResultX,ResultY,"Robot"+i);
		 }
		 }
		 for(int i=0;i<10;i++)
		 {	
			 r[i].run();
		 }
	}
}	
	 



