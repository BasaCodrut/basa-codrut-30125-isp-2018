package ex3;
public class Counter extends Thread {
	 
    Counter(String name){
          super(name);
    }

    public void run(){
          for(int i=0;i<5;i++){
                System.out.println(getName() + " i = "+i);
                try {
                      Thread.sleep(100);
                } catch (InterruptedException e) {
                      e.printStackTrace();
                }
          }
          System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
          Counter c1 = new Counter("counter1");
          Counter c2 = new Counter("counter2");
          c1.start();
          c2.start();
           
try {
	c1.join();
	c2.join();
}catch(InterruptedException e) {
e.printStackTrace();
}
        
        
	
	
}
}
