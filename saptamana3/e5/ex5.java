package g30125.Basa.Alexandru.Codrut.l3.e5;
import becker.robots.*;
public class ex5 {
	public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	    Thing parcel = new Thing(prague, 2, 2);
	      Wall blockAve0 = new Wall(prague, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(prague, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(prague, 1,1, Direction.NORTH);
	      Wall blockAve3 = new Wall(prague, 1,2, Direction.NORTH);
	      Wall blockAve4 = new Wall(prague, 1,2, Direction.EAST);
	      Wall blockAve5 = new Wall(prague, 1,2, Direction.SOUTH);
	      Wall blockAve6 = new Wall(prague, 2,1, Direction.SOUTH);
	  
	      Robot karel = new Robot(prague, 1, 2, Direction.SOUTH);
	 
			
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();	
	      karel.move();
	      karel.pickThing();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.turnLeft();
	      karel.move();
	      karel.putThing();
	      karel.turnLeft();
	      
	    
	
	   }
	} 

