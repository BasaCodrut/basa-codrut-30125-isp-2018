package g30125.Basa.Alexandru.Codrut.l3.e6;

public class MyPoint {
   private int x;
    private int y;
        public MyPoint() {
      x=0;
      y=0;
    }
    public MyPoint(int a, int b) {
       x = a;
       y = b;
    }
    public double getX() {
           return x;
        }
    
    public double getY() {
           return y;
        }
    public int setX(int a)
    {
    	return this.x=a;
    }
    public int setY(int b)
    {
    	return this.y=b;
    }
        public void setXY(int a, int b)
    {
    	this.x=a;
    	this.y=b;
    }
      void ShowXY() {
        System.out.println("("+x+","+y+")");
    } 
    public double distance(int a, int b) {
           return Math.sqrt(
            (a - this.x)*(a - this.x) +
            (b - this.y)*(b - this.y));
    }
    public double distance(MyPoint c)
    {
    	return Math.sqrt((Math.pow(c.x-x, 2)+Math.pow(c.y-y, 2)));
    }
    public String toString() {
        return String.format("(%f, %f)", this.x, this.y);
    }
    

    
   
}
