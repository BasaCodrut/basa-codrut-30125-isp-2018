package ex4;

import java.io.Serializable;

public class Car implements Serializable {
	String model;
    int price;

    public Car(String model,int price) {
        this.model = model;
        this.price=price;
    }
    public String toString(){return "[CarModel="+model+" Price="+price+"]";}
}
