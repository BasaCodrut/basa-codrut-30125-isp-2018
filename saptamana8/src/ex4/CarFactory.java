package ex4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CarFactory {
	Car createCar(String model,int price){
        Car z = new Car(model,price);
        System.out.println(z+"-Car created");
        return z;
    }

    void saveCar(Car a, String storeRecipientName) throws IOException{
        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream(storeRecipientName));

        o.writeObject(a);
        System.out.println(a+"-Car saved");
    }

    Car takeCar(String storeRecipientName) throws IOException, ClassNotFoundException{
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(storeRecipientName));
        Car x = (Car)in.readObject();
        System.out.println("Model:"+x.model+" - Price:"+x.price);
        return x;
    }

}
