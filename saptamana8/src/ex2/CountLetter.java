package ex2;

	import java.io.BufferedReader;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.nio.file.Files;
	import java.nio.file.Path;

	public class CountLetter{
	    
	    private Path fisier;
	    private char caracter;

	    CountLetter(Path fis, char carac){
	        this.fisier = fis;
	        this.caracter = carac;
	    }

	    public int count(){
	        int k = 0;
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(fisier)))){
	            String l = null;
	            while((l = br.readLine()) != null){
	                for(int i = 0; i < l.length(); i++){
	                    if(l.charAt(i) == caracter){
	                        k++;
	                    }
	                }
	            }
	        } catch (IOException x){
	            System.out.println(x);
	        }
	        return k;
	    }
	}

