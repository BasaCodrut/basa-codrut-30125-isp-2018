package ex1;

 class NumberException extends Exception {
	int nr;
	public NumberException(int nr,String msg) {
		super(msg);
		this.nr=nr;
	}

	int getNumber() {
		return this.nr;
	}
}
 