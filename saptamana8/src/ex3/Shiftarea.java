package ex3;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public class Shiftarea {

	private Path fisier;
	public Shiftarea(Path fis) {
		this.fisier=fis;
	}
	
	public void decrypt() {
		String decFinal = new String();
		String dec=new String();
		try(BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(fisier)))){
            String l = new String();
            while((l = br.readLine()) != null) {
                    dec += l +"\n";
            }
            for (int index = 0; index < dec.length();
                    index++) {
                   char a = dec.charAt(index);
                   if(a!='\n'&&a!=' ')
                       a--;
                   decFinal+=a;
               }
            br.close();
        } catch (IOException x){
            System.out.println(x);
        }
		try {
			PrintWriter writer = new PrintWriter("Dec.txt", "UTF-8");
			writer.println(decFinal);
			writer.close();
                      
          } catch(IOException e) {
            throw new RuntimeException(e);
          }
	}
		public void encrypt() {
		String encFinal = new String();
		String enc=new String();
		try(BufferedReader br = new BufferedReader(new InputStreamReader(Files.newInputStream(fisier)))){
            String l = new String();
            while((l = br.readLine()) != null) {
                    enc += l +"\n";
            }
            for (int index = 0; index < enc.length();
                    index++) {
                   char a = enc.charAt(index);
                   if(a!='\n'&&a!=' ')
                       a++;
                   encFinal+=a;
               }
            br.close();
        } catch (IOException x){
            System.out.println(x);
        }
		try {
			PrintWriter writer = new PrintWriter("Enc.txt", "UTF-8");
			writer.println(encFinal);
			writer.close();
             } catch(IOException e) {
            throw new RuntimeException(e);
          }
	}
	
	
}
