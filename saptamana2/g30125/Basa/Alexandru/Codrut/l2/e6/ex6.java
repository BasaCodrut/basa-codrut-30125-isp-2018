package g30125.Basa.Alexandru.Codrut.l2.e6;
import java.util.Scanner;
public class ex6 {
	   static void nerec(int n){
	       int suma=1;
	       for(int i=1;i<=n;i++)
	           suma=i*suma;
	       System.out.println("N!= "+suma);
	   }
	   static int rec(int n){
	       if(n==1)    return 1;
	       else  return n * rec(n-1);
	   }
	   public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       System.out.println("Valoarea lui N:");
	       int n=in.nextInt();
	       int m=n;
	       System.out.println("N! calculat nerecursiv: ");
	       nerec(n);
	       int k = rec(m);
	       System.out.println("N! calculat recursiv: "+k);
	   }
	}
