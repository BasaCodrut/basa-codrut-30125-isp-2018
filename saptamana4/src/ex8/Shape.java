package ex8;

public class Shape {
	
	private String color;
	private boolean filled;
	
	public Shape() {
		this.color="green";
		this.filled=true;
	}
	
	public Shape(String col,boolean bool) {
		this.color=col;
		this.filled=bool;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public void setColor(String col) {
		this.color=col;
	}
	
	public boolean isFilled() {
		return this.filled;
	}
	
	public void setFilled(boolean fill) {
	    this.filled=fill;	
	}
	
	public String toString() {
		return "A Shape with "+getColor()+" and "+isFilled();
	}
	

}
