package ex8;

public class Rectangle extends Shape {
	
	private double width;
	private double length;
	
	public Rectangle() {
		this.width = 1.0;
		this.length = 1.0;
	}
	
	public Rectangle(double widt,double leng) {
		this.width=widt;
		this.length=leng;
	}
	
	public Rectangle(double widt,double leng,String color,boolean filled) {
		super(color,filled);
		this.width=widt;
		this.length=leng;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public void setWidth(double widt) {
		this.width=widt;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public void setLength(double leng) {
		this.length=leng;
	}
	
	public double getArea() {
		return width*length;
	}
	
	public double getPerimeter() {
		return 2*(width*length);
	}
	
	public String toString() {
		super.toString();
		return "A Rectangle with width="+getWidth()+" and length="+getLength()+",which is a subclas of "+super.toString()+" method from the superclass";
	}
	

}
