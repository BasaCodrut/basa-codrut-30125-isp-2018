package ex6;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import ex5.Book;
import ex4.Author;
import ex6.BookWithNAuthors;

public class TestBookWithNAuthors {
		@Test
		public void testToString() {
			Author[] author = new Author[2];
			author[0]=new Author("author","author@yahoo.com",'F');
					
			author[1]=new Author("author1","author1@yahoo.com",'M');
			BookWithNAuthors b  = new BookWithNAuthors("cartile",author,23,75);
			System.out.println(b.toString());
			
		}
		
		@Test
		public void testPrintAuthors() {
			Author[] author = new Author[2];
			author[0]=new Author("author","author@yahoo.com",'F');
					
			author[1]=new Author("author1","author1@yahoo.com",'M');
			BookWithNAuthors b1 = new BookWithNAuthors("booktitle",author,23,75);
			b1.printAuthors();
			}
}
