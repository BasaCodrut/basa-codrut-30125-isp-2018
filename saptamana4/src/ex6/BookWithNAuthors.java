package ex6;

import ex4.Author;

public class BookWithNAuthors {

	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock;
	
	public BookWithNAuthors() {
		this.name="Cartea";
		authors[0]=new Author("Codrut","codrut@gmail.com",'M');
		authors[1]=new Author("Basa","basax@gmail.com",'M');
		this.price= 5.8;
		this.qtyInStock=25;
		}
	
	public BookWithNAuthors(String nam,Author[] auth,double pri,int qty) {
		this.name=nam;
		this.authors=auth;
		this.price= pri;
		this.qtyInStock=qty;
				
	}
	
	public String getName() {
		return this.name;
	}
	
	public Author[] getAuthors() {
		return this.authors;
	}
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double pri) {
		this.price=pri;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qty) {
		this.qtyInStock=qty;
	}
	
	public void printAuthors() {
		for(Author a:authors) {
			System.out.println(a.getName());
		}
	}
	
	public String toString() {
		return " book name " + this.name +" by " + this.authors.length ;	
		}
	
}
