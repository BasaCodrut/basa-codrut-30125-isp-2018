package ex7;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import ex3.Circle;
public class TestCylinder {
	
	@Test
	public void testGetHeight() {
		Cylinder c1 = new Cylinder();
		Cylinder c2 = new Cylinder(12.6);
		Cylinder c3 = new Cylinder(10.5,17);
		
		assertEquals(1.0,c1.getHeight(),4);
		assertEquals(10,c2.getHeight(),4);
		assertEquals(14.0,c3.getHeight(),4);
		
	}@Test
	public void testGetVolume() {
		Cylinder c = new Cylinder(12.0,25.0);
		assertEquals(628.318,c.getVolume(),4);
	}

}
