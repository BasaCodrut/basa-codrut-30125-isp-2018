package ex7;
import ex3.Circle;

public class Cylinder extends Circle {
	
	private double height;
	
	public Cylinder() {
		this.height=1.0;
	}
	
	public Cylinder(double radius) {
		super(radius,"red");
		this.height=10;
	}
	
	public Cylinder(double radius, double height) 
	{
		super(radius,"red");
		this.height=height;
	}
	
	public double getHeight() 
	{
		return height;
	}
	
	public double getArea() 
	{
		super.getArea();
		return Math.PI*super.getRadius()*super.getRadius();
	}
	public double getVolume() 
	{
		return getHeight()*getArea();
	}

}
