package ex2;


public class MyPoint {

	private int x;
    private int y;
    
    /*Constructor with no arguments*/
    public MyPoint() {
      x=0;
      y=0;
    }
    
	/*Constructor with arguments*/
    public MyPoint(int a, int b) {
       x = a;
       y = b;
    }
    /* Getter And Setter */
    public double getX() {
           return x;
        }
    
    public double getY() {
           return y;
        }
    public int setX(int a)
    {
    	return this.x=a;
    }
    public int setY(int b)
    {
    	return this.y=b;
    }
    
    /*setXY*/
    public void setXY(int a, int b)
    {
    	this.x=a;
    	this.y=b;
    }
    /*Show XY*/
    void ShowXY() {
        System.out.println("("+x+","+y+")");
    }
    
    /* Distance between two points*/
    public double distance(int a, int b) {
       
        return Math.sqrt(
            (a - this.x)*(a - this.x) +
            (b - this.y)*(b - this.y));
    }
    
    
    /*Overloaded distance*/
    public double distance(MyPoint c)
    {
    	return Math.sqrt((Math.pow(c.x-x, 2)+Math.pow(c.y-y, 2)));
    }
 
    
    
    public String toString() {
        return String.format("(%f, %f)", this.x, this.y);
    }
    

    
   
}






