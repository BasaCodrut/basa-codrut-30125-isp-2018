package ex3;

public class Circle {
	
	private double radius;
	private String color;
	private static double pi=3.14;
	
	public Circle() {
		this.radius=1.0;
		this.color="red";
	}
	public Circle(double rad, String col) {
		this.radius=rad;
		this.color=col;
	}
	
	public double getRadius() {
		return this.radius;
		
	}
	
	public double getArea() {
		return 2*pi*this.radius;
	}
	
	

}
