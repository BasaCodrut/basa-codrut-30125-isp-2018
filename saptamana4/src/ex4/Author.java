package ex4;

public class Author {
	
	private String name;
	private String email;
	private char gender;
	
	public Author(String nam,String emai,char gen) {
		this.name=nam;
		this.email=emai;
		this.gender=gen;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String nam) {
		this.name=nam;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String mai) {
		this.email=mai;
	}
	
	public char getGender() {
		   return this.gender;
	}
	
	public String toString() {
		return this.name +"( " + this.gender+ ")"+ " at "+ this.email;
	}

}
