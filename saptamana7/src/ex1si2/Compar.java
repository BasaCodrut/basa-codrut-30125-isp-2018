package ex1si2;

import java.util.Comparator;

public class Compar implements Comparator<BankAccount>{	 
	    @Override
	    public int compare(BankAccount e1, BankAccount e2) {
	        if(e1.getBalance() > e2.getBalance()){
	            return 1;
	        } else {
	            return -1;
	        }
	    }
	}

