package ex1si2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Main {

	public static void main(String[] args) {
		//Ex 1
		BankAccount a1 = new BankAccount("Codrut",100);
		BankAccount b1 = new BankAccount("Codrut",100);
		
		System.out.println(a1.equals(b1));
		System.out.println(a1.hashCode());
		System.out.println(b1.hashCode());
		
		
		
		//Ex 2
		Bank c1 = new Bank();
		c1.addAccount("Basa", 1);
		c1.addAccount("Alexandru", 2);
		c1.addAccount("Codrut", 3);
		
		 System.out.println("Toate conturile(sortate):");
		c1.printAccounts();
		
		System.out.println("-----\n Conturile intre 2 si 3 :");
		c1.printAccounts(2, 3);
		
		c1.getAccount("Basa");
		
	


	}

	
}
