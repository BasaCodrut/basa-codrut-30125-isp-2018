package ex1si2;

import java.util.Objects;

public class BankAccount {

	public String owner;
	public double balance;
	
	public BankAccount() {
		this.owner="";
		this.balance=0;
	}
	public BankAccount(String owner,double balance) {
		this.owner=owner;
		this.balance=balance;
	}
	
	public void withdraw(double amount) {
		double a;
		a=getBalance();
		if(a>amount) {
			a=a-amount;
			setBalance(a);
			System.out.println("Finish!");
		}
		else System.out.println("Not enough money!");
	}
	
	public void deposit(double amount) {
		double a;
		a=getBalance();
		a=a+amount;
		setBalance(a);
		System.out.println("Finish!");
	}
	public double getBalance() {
		return this.balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public String getOwner() {
		return this.owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	@Override
	public boolean equals (Object obiect) {
		if(obiect instanceof BankAccount) {
			BankAccount b = (BankAccount)obiect;
			return (balance == b.balance && owner.equals(b.owner));
		}
		return false;
	}
	
	
	 @Override
	    public int hashCode() {
	        return Objects.hash(owner, balance);
	    }
	 
}
	
