package ex4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	public static void main(String args[]) throws Exception {
        Dictionary dict = new Dictionary();
        char raspuns;
        String linie, explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.println("Meniu");
            System.out.println("A - Adauga cuvant");
            System.out.println("D - Listeaza definitii");
            System.out.println("C - Listeaza cuvinte");
            System.out.println("X - Iesi");
            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);
            switch(raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if( linie.length()>1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'c': case 'C':
                    System.out.println("Afisare cuvinte:");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Afisare definitii:");
                    dict.getAllDefinitions();
                    break;
            }
        } while(raspuns!='x' && raspuns!='X');
        System.out.println("Program terminat.");
    }
}


