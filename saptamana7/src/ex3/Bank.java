package ex3;

import java.util.*;

public class Bank extends BankAccount {

    private static TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(new MyValComp());
    private static TreeSet<BankAccount> accounts2 = new TreeSet<BankAccount>(new MyOwnerComp());

    static class MyOwnerComp implements Comparator<BankAccount>
    {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
           return e1.getOwner().compareTo(e2.getOwner());
        }
    }
    static class MyValComp implements Comparator<BankAccount>
    {
        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            if(e1.getBalance() > e2.getBalance()){
                return 1;
            } else {
                return -1;
            }
        }
    }
    public static void getAllAcounts() 
    {
        for(Object o:accounts2){
            System.out.println(o.toString());
        }
    }
    public static void addAcount(String owner,double balance)
    {
        accounts.add(new BankAccount(owner,balance)); 
        accounts2.add(new BankAccount(owner,balance)); 
    }
    public static void printAccounts(double minBalance, double maxBalance)
    {
        System.out.println("\n Accounts between "+minBalance+" and "+maxBalance+" are:");
        Iterator<BankAccount> iterator = accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount node = iterator.next();
            if(node.getBalance() > minBalance && node.getBalance()<maxBalance)
                System.out.println(node.toString());
        }
    }
    public static void printAccounts()
    {

        for(Object o:accounts){
            System.out.println(o.toString());
        }
    }
    public static BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator = accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount node = iterator.next();
            if (node.getOwner().equals(owner)) {
                System.out.println("\nAccount found!");
                return node;
            }
        }
        System.out.println("\n Account with name :"+owner+" was not found!");
        return null;
    }
    public static void main(String[] args)
    {
    	 addAcount("Cioba",264);
         addAcount("Adrian",234);
         addAcount("Andrei",179);
        printAccounts();
        printAccounts(10,100);
        System.out.println("\n");
        getAllAcounts();
        System.out.println(getAccount("Andrei"));
        System.out.println(getAccount("Cioba"));
    }

}

